import numpy as np

from . import _timepix3

t3p_dtype = np.dtype(
    [
        ("matrixIdx", np.uint32),
        ("toa", np.uint64),
        ("overflow", np.uint8),
        ("ftoa", np.uint8),
        ("tot", np.uint16),
    ]
)


def process_events(event_list, min_time, max_time, n_bins):
    histogram_raw = _timepix3.process_events(event_list, min_time, max_time, n_bins)
    histogram = histogram_raw.reshape([n_bins, _timepix3.ny, _timepix3.nx])
    
    bin_edges = np.linspace(min_time, max_time, n_bins+1, endpoint=True)
    bin_centers = 0.5 * (bin_edges[:-1] + bin_edges[1:])

    return bin_centers, histogram


def read_t3p(filename):
    return np.fromfile(filename, dtype=t3p_dtype)


def process_t3p(filename, min_time=0, max_time=1e6, n_bins=1000):
    event_list = read_t3p(filename)
    histogram = process_events(event_list, min_time, max_time, n_bins)

    return histogram
