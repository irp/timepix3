#pragma once

#include <Eigen/Dense>
#include <vector>
#include <iostream>

namespace tpx3 {

using CountScalar = uint16_t;
using TimeScalar = double;
using Histogram = Eigen::Array<CountScalar, Eigen::Dynamic, Eigen::Dynamic>;

const int nx = 256;  // number of columns
const int ny = 256;  // number of rows
constexpr int index_max = ny * nx;

template <typename T>
class indexmap {
 public:
  const T min, max;
  const int num_bins;
  const T reciprocal_width;

  indexmap(T min, T max, Eigen::Index num_bins)
      : min(min),
        max(max),
        num_bins(num_bins),
        reciprocal_width(static_cast<T>(num_bins) / (max - min)) {}

  Eigen::Index operator()(T x) const {
    return std::floor((x - min) * reciprocal_width);
  }
};

struct T3pEvent {
  uint32_t matrixIdx;
  uint64_t toa;
  uint8_t overflow;
  uint8_t ftoa;
  uint16_t tot;

  TimeScalar time() const {
      return static_cast<double>(toa) * 25. - static_cast<double>(ftoa) * 25. / 16.;
  }
};


Histogram process_events(const std::vector<T3pEvent> & event_list, const double time_min, const double time_max, const int num_bins) {
  Histogram hist = Histogram::Zero(num_bins, index_max);
  const indexmap<TimeScalar> map { time_min, time_max, num_bins };

  size_t count = 0;

  for (auto const event : event_list) {
    Eigen::Index matrixIdx = event.matrixIdx;
    // toa in ns
    TimeScalar time = event.time();
    Eigen::Index timeIdx = map(time);

    ++hist(timeIdx, matrixIdx);
    ++count;
  }

  std::cout << "processed " << count << " events." << std::endl;

  return hist;
}


}  // namespace timepix3
