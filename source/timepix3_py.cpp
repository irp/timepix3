#include <pybind11/eigen.h>
#include <pybind11/iostream.h>
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>

#include "timepix3.h"

namespace py = pybind11;

template <typename T>
std::vector<T> array_to_list(const py::array_t<T> & in) {
  py::buffer_info buf1 = in.request();

    if (buf1.ndim != 1)
        throw std::runtime_error("Number of dimensions must be one");

    size_t size = buf1.shape[0];

    T *ptr1 = static_cast<T *>(buf1.ptr);
    std::vector<T> out (ptr1, &(ptr1[size-1]));

    return out;
}
tpx3::Histogram wrap_process_events(const py::array_t<tpx3::T3pEvent> & event_list, const double time_min, const double time_max, const int num_bins) {
    return tpx3::process_events(array_to_list(event_list), time_min, time_max, num_bins);
}


PYBIND11_MODULE(_timepix3, m) {
  m.doc() = "TIMEPIX3 processor";

  m.attr("nx") = py::int_(tpx3::nx);
  m.attr("ny") = py::int_(tpx3::ny);

  PYBIND11_NUMPY_DTYPE(tpx3::T3pEvent, matrixIdx, toa, overflow, ftoa, tot);

  py::class_<tpx3::T3pEvent>(m, "T3pEvent")
    .def(py::init<>())
    .def_readwrite("matrixIdx", &tpx3::T3pEvent::matrixIdx)
    .def_readwrite("toa", &tpx3::T3pEvent::toa)
    .def_readwrite("overflow", &tpx3::T3pEvent::overflow)
    .def_readwrite("ftoa", &tpx3::T3pEvent::ftoa)
    .def_readwrite("tot", &tpx3::T3pEvent::tot);

  m.def("process_events",
       &wrap_process_events,
       "process list of events",
       py::arg("event_list"),
       py::arg("time_min"),
       py::arg("time_max"),
       py::arg("num_bins"),
       py::call_guard<py::scoped_ostream_redirect>(),
       py::return_value_policy::move
   );

  m.def("process_events_raw",
       &tpx3::process_events,
       "process list of events",
       py::arg("event_list"),
       py::arg("time_min"),
       py::arg("time_max"),
       py::arg("num_bins"),
       py::call_guard<py::scoped_ostream_redirect>(),
       py::return_value_policy::move
   );
}
